﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AnsermaAir.Models
{
    public class AnsermaAirContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AnsermaAirContext() : base("name=AnsermaAirContext")
        {
        }

        public System.Data.Entity.DbSet<AnsermaAir.Models.Passenger> Flights { get; set; }

        public System.Data.Entity.DbSet<AnsermaAir.Models.Flight> Passengers { get; set; }

        public System.Data.Entity.DbSet<AnsermaAir.Models.Bag> Bags { get; set; }

        public System.Data.Entity.DbSet<AnsermaAir.Models.Service> Services { get; set; }

        public System.Data.Entity.DbSet<AnsermaAir.Models.Seat> Seats { get; set; }
    }
}
