﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsermaAir.Models
{
    public class Passenger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public List<Passenger> Passengers { get; set; }

        public virtual Flight Flight { get; set }
    }
}