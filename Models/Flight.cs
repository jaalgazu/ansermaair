﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsermaAir.Models
{
    public class Flight
    {
        public int Id{ get; set; }
        public DateTime DepartureDateTime { get; set; }
        public string DepartureStation { get; set; }
        public string ArrivalStation { get; set; }

        public List<Passenger> Passengers { get; set; }

        public virtual ICollection<Passenger> Passengers { get;set }
        internal object ObtenerFlight()
        {
            throw new NotImplementedException();
        }
    }
}