namespace AnsermaAir.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Passengers", "Flight_Id", c => c.Int());
            CreateIndex("dbo.Passengers", "Flight_Id");
            AddForeignKey("dbo.Passengers", "Flight_Id", "dbo.Flights", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Passengers", "Flight_Id", "dbo.Flights");
            DropIndex("dbo.Passengers", new[] { "Flight_Id" });
            DropColumn("dbo.Passengers", "Flight_Id");
        }
    }
}
